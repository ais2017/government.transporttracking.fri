package AIS.Government.objects;

/**
 * Created by ruslbizh on 03.11.2017.
 */
public class Trolleybus extends Transport{

    private static final double speedCoef = 1.2;
    private int timeBetweenStops;
    private int generalTimeOfTraffic;

    public Trolleybus(String transportName) {
        setTransportName(transportName);
    }

    public static double getSpeedCoef() {
        return speedCoef;
    }

    @Override
    public int getTimeBetweenStops() {
        return timeBetweenStops;
    }

    @Override
    public void setTimeBetweenStops(int generaltimeBetweenStops, int numOfStops){
        this.timeBetweenStops = (int)(generaltimeBetweenStops/speedCoef);
        this.generalTimeOfTraffic = timeBetweenStops*numOfStops;
    }

    @Override
    public int getGeneralTimeOfTraffic() {
        return generalTimeOfTraffic;
    }

}