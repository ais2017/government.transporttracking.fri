package AIS.Government.businessLogic;

import AIS.Government.objects.Route;
import AIS.Government.objects.Stop;
import AIS.Government.objects.Transport;

import java.security.InvalidParameterException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by ruslbizh on 19.11.2017.
 */
public class RouteCharacteristicsImpl implements RouteCharacteristics {

    private List<Route> routes;

    public RouteCharacteristicsImpl(List<Route> routes) {
        this.routes = routes;
    }

    public String getCurrentTime(){
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        return sdf.format(cal.getTime());
    }

    public List<Stop> getRouteForTransport(String transportName){
        for(Route route: routes){
            if(route.getTransport(transportName) != null){
                return route.getStops();
            }
        }
        return null;
    }

    public Map<String, String> getTransportsTimeToStop(String stopName, String curTime){
        TreeMap<String, String> map = new TreeMap<String, String>();
        for(Route route: routes){
            if(route.getStop(stopName) != null){
                ArrayList<Transport> transports = route.getTransports();
                for(Transport transport: transports){
                    int time = route.getTimeToStop(transport.getTransportName(),stopName, curTime);
                    String stringTime = RouteCharacteristics.getStringTime(time);
                    map.put(stringTime, transport.getTransportName());
                }
            }
        }
        return map;
    }
}
