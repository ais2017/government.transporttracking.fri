package AIS.Government.dao;

import AIS.Government.objects.Route;

import java.util.List;

/**
 * Created by ruslbizh on 03.12.2017.
 */
public class RoutesDaoImpl implements RoutesDao{

    @Override
    public List<Route> requestForRoutesByStop(String stopName) {//returns routes from DataBase which contain this Stop
        return null;
    }

    @Override
    public List<Route> requestForRoutesByTransport(String transportName) {//returns routes from DataBase which contain this Transport
        return null;
    }
}
