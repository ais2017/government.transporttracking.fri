package AIS.Government.dao;

import AIS.Government.objects.Route;

import java.util.List;

/**
 * Created by ruslbizh on 03.12.2017.
 */
public interface RoutesDao {
    List<Route> requestForRoutesByStop(String stopName);
    List<Route> requestForRoutesByTransport(String transportName);
}
