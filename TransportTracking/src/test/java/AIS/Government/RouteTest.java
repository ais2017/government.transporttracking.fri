package AIS.Government;

import AIS.Government.objects.*;
import org.junit.*;
import org.junit.Assert;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.*;
import java.security.InvalidParameterException;
import java.util.ArrayList;

/**
 * Created by ruslbizh on 04.11.2017.
 */


public class RouteTest {

    private Route route;
    private ArrayList<String> correctStarts;
    private ArrayList<String> wrongStarts;
    private ArrayList<Transport> transports;
    private ArrayList<Stop> stops;
    private ArrayList<String> starts;

    @Before
    public void init() {
        route = new Route("firstRoute");
        stops = new ArrayList<Stop>();
        stops.add(new Stop("stop1"));
        stops.add(new Stop("stop2"));
        stops.add(new Stop("stop3"));
        correctStarts = new ArrayList<String>();
        correctStarts.add("16:12:00");
        correctStarts.add("09:58:00");
        wrongStarts = new ArrayList<String>();
        wrongStarts.add("9:12:00");
        wrongStarts.add("16:55");
    }

    @Test
    public void testCheckStarts(){
        Assert.assertEquals(false, route.checkStarts(wrongStarts));
        Assert.assertEquals(true, route.checkStarts(correctStarts));
        transports = new ArrayList<Transport>();
        transports.add(new Trolleybus("trolleybus"));
        route.setTransports(transports);
        Assert.assertEquals(false, route.checkStarts(correctStarts));
    }

    public void initTransports(){
        transports = new ArrayList<Transport>();
        transports.add(new Trolleybus("trolleybus"));
        transports.add(new Tram("tram"));
        transports.add(new Bus("bus"));
    }

    @Test
    public void testSetTransports(){
        initTransports();
        route.setTransports(transports);
        Assert.assertEquals(0, route.getTransports().get(0).getStartNumber());
        Assert.assertEquals(1, route.getTransports().get(1).getStartNumber());
        Assert.assertEquals(2, route.getTransports().get(2).getStartNumber());
    }

    @Test(expected = InvalidParameterException.class)
    public void testSetTransportsException(){
        route.setGeneralTimeBetweenStops(1440);
        route.setTransports(new ArrayList<Transport>());
        starts = new ArrayList<String>();
        starts.add("06:15:00");
        route.setStarts(starts);
        initTransports();
        route.setTransports(transports);
    }

    @Test
    public void testSetStops(){
        initTransports();
        route.setGeneralTimeBetweenStops(1440);
        route.setTransports(transports);
        route.setStops(stops);
        Assert.assertEquals(3, route.getNumOfStops());
        int tmp = (int)(1440/(Trolleybus.getSpeedCoef()))*3;
        Assert.assertThat(route.getTransports().get(0).getGeneralTimeOfTraffic(), is(tmp));
        tmp = (int)(1440/(Tram.getSpeedCoef()))*3;
        Assert.assertThat(route.getTransports().get(1).getGeneralTimeOfTraffic(), is(tmp));
        tmp = (int)(1440/(Bus.getSpeedCoef()))*3;
        Assert.assertThat(route.getTransports().get(2).getGeneralTimeOfTraffic(), is(tmp));
    }

    @Test
    public void testSetGeneralTimeOfTraffic(){
        initTransports();
        route.setTransports(transports);
        route.setGeneralTimeBetweenStops(2880);
        int tmp = (int)(2880/(Trolleybus.getSpeedCoef()));
        Assert.assertThat(route.getTransports().get(0).getTimeBetweenStops(), is(tmp));
        tmp = (int)(2880/(Tram.getSpeedCoef()));
        Assert.assertThat(route.getTransports().get(1).getTimeBetweenStops(), is(tmp));
        tmp = (int)(2880/(Bus.getSpeedCoef()));
        Assert.assertThat(route.getTransports().get(2).getTimeBetweenStops(), is(tmp));
    }

    @Test
    public void testAddMethods(){
        initTransports();
        route.setTransports(transports);
        route.setStops(stops);
        correctStarts.add("09:58:22");
        route.setStarts(correctStarts);
        int prevT = route.getNumOfTransports();
        int prevStops = route.getNumOfStops();
        int prevStarts = route.getStarts().size();
        route.addStart("06:14:55");
        route.addStop(new Stop("adasdad"));
        route.addTransport(new Tram("asdsads"));
        Assert.assertEquals(prevT+1, route.getNumOfTransports());
        Assert.assertEquals(prevStops+1, route.getNumOfStops());
        Assert.assertEquals(prevStarts+1, route.getStarts().size());
    }

    @Test
    public void testDelMethods(){
        initTransports();
        route.setTransports(transports);
        route.setStops(stops);
        correctStarts.add("09:58:22");
        route.setStarts(correctStarts);
        int prevT = route.getNumOfTransports();
        int prevStops = route.getNumOfStops();
        int prevStarts = route.getStarts().size();
        route.delStart(0);
        route.delStop("stop3");
        route.delTransport("tram");
        Assert.assertEquals(prevT-1, route.getNumOfTransports());
        Assert.assertEquals(prevStops-1, route.getNumOfStops());
        Assert.assertEquals(prevStarts-1, route.getStarts().size());
    }

    @Test
    public void testSetMethods(){
        initTransports();
        route.setTransports(transports);
        route.setStops(stops);
        correctStarts.add("09:58:22");
        route.setStarts(correctStarts);
        int prevT = route.getNumOfTransports();
        int prevStops = route.getNumOfStops();
        int prevStarts = route.getStarts().size();
        route.setStart(0, "09:58:55");
        Stop s = new Stop("stop1");
        s.setLatitude(2);
        route.setStop(s);
        Tram t = new Tram("tram");
        t.setStartNumber(10);
        route.setTransport(t);
        Assert.assertEquals("09:58:22", route.getStart(0));
        Assert.assertEquals(2, (int)route.getStop("stop1").getLatitude());
        Assert.assertEquals(10, route.getTransport("tram").getStartNumber());
    }

    @Test
    public void testGetTimeToNextStop(){
        String startTime = "06:00:00";
        starts = new ArrayList<String>();
        starts.add(startTime);
        route.setStarts(starts);
        transports = new ArrayList<Transport>();
        Transport transport = new Trolleybus("trolleybus");
        transports.add(transport);
        route.setGeneralTimeBetweenStops(1080);//18 min -> 15 min at trolleybus
        route.setTransports(transports);
        route.setStops(stops);//3 stops
        String curTime = "07:17:00";
        Assert.assertEquals(route.getTimeToNextStop(route.getTransports().get(0).getTransportName(), curTime), new Integer(780));
        curTime = "07:43:00";
        Assert.assertEquals(route.getTimeToNextStop(route.getTransports().get(0).getTransportName(), curTime), new Integer(120));
        curTime = "07:02:00";
        Assert.assertEquals(route.getTimeToNextStop(route.getTransports().get(0).getTransportName(), curTime), new Integer(780));
        curTime = "08:15:00";
        Assert.assertEquals(route.getTimeToNextStop(route.getTransports().get(0).getTransportName(), curTime), new Integer(0));
    }

    @Test
    public void testGetTimeToStop(){
        String startTime = "06:00:00";
        starts = new ArrayList<String>();
        starts.add(startTime);
        route.setStarts(starts);
        transports = new ArrayList<Transport>();
        Transport transport = new Trolleybus("trolleybus");
        transports.add(transport);
        route.setGeneralTimeBetweenStops(1080);//18 min -> 15 min at trolleybus
        route.setTransports(transports);
        route.setStops(stops);//3 stops
        String curTime = "06:05:00";
        Assert.assertEquals(route.getTimeToStop(route.getTransports().get(0).getTransportName(),"stop1", curTime), new Integer(2400));
        Assert.assertEquals(route.getTimeToStop(route.getTransports().get(0).getTransportName(),"stop2", curTime), new Integer(600));
        Assert.assertEquals(route.getTimeToStop(route.getTransports().get(0).getTransportName(),"stop3", curTime), new Integer(1500));
        curTime = "08:15:00";
        Assert.assertEquals(route.getTimeToStop(route.getTransports().get(0).getTransportName(),"stop3", curTime), new Integer(1800));
        curTime = "08:16:00";
        Assert.assertEquals(route.getTimeToStop(route.getTransports().get(0).getTransportName(),"stop3", curTime), new Integer(1740));
    }
}
