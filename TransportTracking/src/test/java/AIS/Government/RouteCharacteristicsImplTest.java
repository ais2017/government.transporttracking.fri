package AIS.Government;

import AIS.Government.businessLogic.RouteCharacteristics;
import AIS.Government.businessLogic.RouteCharacteristicsImpl;
import AIS.Government.objects.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by ruslbizh on 20.11.2017.
 */
public class RouteCharacteristicsImplTest {

    private List<Route> routes = new ArrayList<Route>();

    @Before
    public void init() {
        Route route;
        route = fillRoute("route1","stop1.1", "stop2.2", "stop1.3", "trolleybus1.1",
                "tram1.2","bus1.3","06:00:00", "06:15:00", "06:30:00");
        routes.add(route);
        route = fillRoute("route2","stop2.1", "stop2.2", "stop2.3", "trolleybus2.1",
                "tram2.2","bus2.3","06:05:00", "06:20:00", "06:35:00");
        routes.add(route);
        route = fillRoute("route3","stop3.1", "stop2.2", "stop3.3", "trolleybus3.1",
                "tram3.2","bus3.3","06:10:00", "06:25:00", "06:40:00");
        routes.add(route);
    }

    private Route fillRoute(String routeName, String stopNm1, String stopNm2, String stopNm3,
                            String trNm1, String trNm2, String trNm3, String startNm1, String startNm2, String startNm3){
        ArrayList<Transport> transports;
        ArrayList<Stop> stops;
        ArrayList<String> starts;
        Route route = new Route(routeName);
        route.setGeneralTimeBetweenStops(1080);
        stops = new ArrayList<Stop>();
        stops.add(new Stop(stopNm1));
        stops.add(new Stop(stopNm2));
        stops.add(new Stop(stopNm3));
        route.setStops(stops);
        transports = new ArrayList<Transport>();
        transports.add(new Trolleybus(trNm1));
        transports.add(new Tram(trNm2));
        transports.add(new Bus(trNm3));
        route.setTransports(transports);
        starts = new ArrayList<String>();
        starts.add(startNm1);
        starts.add(startNm2);
        starts.add(startNm3);
        route.setStarts(starts);
        return route;
    }

    @Test
    public void getStringTimeTest(){
        String time = RouteCharacteristics.getStringTime(22440);
        Assert.assertEquals("06:14:00", time);
    }
    @Test(expected = InvalidParameterException.class)
    public void getStringTimeExceptionTest(){
        String time = RouteCharacteristics.getStringTime(-121);
    }

    @Test
    public void getRouteForTransportTest(){
        RouteCharacteristicsImpl routeCharacteristicsImpl = new RouteCharacteristicsImpl(routes);
        List<Stop> stops = routeCharacteristicsImpl.getRouteForTransport("bus2.3");
        for(int i=0; i < stops.size(); ++i){
            System.out.println("stop = "+stops.get(i).getStopName());
        }
        Assert.assertEquals(3, stops.size());
        Assert.assertEquals("stop2.2", stops.get(1).getStopName());
    }

    @Test
    public void getTransportsTimeToStopTest(){
        RouteCharacteristicsImpl routeCharacteristicsImpl = new RouteCharacteristicsImpl(routes);
        Map<String, String> tmp = routeCharacteristicsImpl.getTransportsTimeToStop("stop2.2", "06:10:00");
        Map.Entry<String,String> timeBus1_3_Actual = null;
        for(Map.Entry<String,String> entry: tmp.entrySet()){
            System.out.println("transport: "+entry.getValue()+" - time: "+entry.getKey());
            if(entry.getValue().equals("bus1.3")) {
                timeBus1_3_Actual = entry;
            }
        }
        Assert.assertEquals(9, tmp.size());
        int timeBus1_3_Expected = (int)(20*60+18*60/1.4);
        Assert.assertEquals("bus1.3", timeBus1_3_Actual.getValue());
        Assert.assertEquals(RouteCharacteristics.getStringTime(timeBus1_3_Expected), timeBus1_3_Actual.getKey());
    }
}
