<%@page language="java"
import="java.text.SimpleDateFormat, java.security.InvalidParameterException, java.util.List, java.util.Map, java.sql.Connection,
java.sql.DriverManager, businessLogic.RouteCharacteristicsImpl, dao.RoutesDaoImpl, objects.Stop, objects.Route"
contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<body>
<% String transportName=request.getParameter("transport"); %>
<% String stopName=request.getParameter("stopname"); %>
<% 
    if (stopName != null && !stopName.isEmpty()) {
      out.println(stopName);
      out.println("------");

      RouteCharacteristicsImpl logic = new RouteCharacteristicsImpl();
      
      String curTime = logic.getCurrentTime();
      Map<String, String> times = logic.getTransportsTimeToStop(stopName, curTime);

      if ( (times == null)||(times.size() == 0) ) {
        out.println("Don't exist this element");
      } else {
        for(Map.Entry<String, String> time : times.entrySet()){
          out.println(time.getKey() + "/" + time.getValue());
        }
      }

    }
%>
</body>
</html>
