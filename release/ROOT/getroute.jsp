<%@page language="java"
import="java.text.SimpleDateFormat, java.security.InvalidParameterException, java.util.List, java.util.*, java.sql.Connection,
java.sql.DriverManager, businessLogic.RouteCharacteristicsImpl, objects.Stop, objects.Route"
contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<body>
<% String transportName=request.getParameter("transport"); %>
<% 
//    String utfTransportName = new String (transportName.getBytes ("UTF-8"), "ISO-8859-1");
    if (transportName != null && !transportName.isEmpty()) {
      out.println(transportName);
      out.println("------");

      RouteCharacteristicsImpl logic = new RouteCharacteristicsImpl();
      
      List<Stop> stops = logic.getRouteForTransport(transportName);

      if ( (stops == null)||(stops.size() == 0) ) {
        out.println("Don't exist this element");
      } else {
        for(Stop stop: stops){
          out.println( stop.getStopName() );
        }
      }

    }

%>
</body>
</html>
