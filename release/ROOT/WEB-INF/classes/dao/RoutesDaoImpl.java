package dao;

import objects.Route;
import objects.Stop;
import objects.Transport;
import objects.Bus;

import java.util.ArrayList;

import java.util.logging.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.ResultSet;

/**
 * Created by ruslbizh on 03.12.2017.
 */
public class RoutesDaoImpl implements RoutesDao {

  public ArrayList<Route> requestForRoutesByTransport(String transportName) {
    //@Override

    Logger log = Logger.getLogger("RoutesDaoImpl");        
    log.info("FA:simple");

    Connection c = null;
    Statement stmtTransport = null;
    Statement stmtStop = null;
    Statement stmtTime = null;
    Statement stmtRoute = null;
    ArrayList<Route> routes = new ArrayList<Route>();

    try {
      log.info("FA:Before connect");
      Class.forName("org.postgresql.Driver");
      log.info("FA:After forName");
      c = DriverManager.getConnection("jdbc:postgresql://localhost:5433/goverment", "alex", "password");
      c.setAutoCommit(false);
      log.info("FA:After connect");

      log.info("FA:Before Statement");
      stmtTransport = c.createStatement();
      stmtStop = c.createStatement();
      stmtTime = c.createStatement();
      stmtRoute = c.createStatement();
      log.info("FA:After Statement");      

      String selTransport = "SELECT v.route_number, v.transport_name  FROM ( (SELECT route_number FROM transport, transport_in_route WHERE     transport.transport_number=transport_in_route.transport_number AND transport.transport_name='";

      selTransport += transportName;

      selTransport += "') AS u INNER JOIN (SELECT route_number, transport_name FROM transport, transport_in_route WHERE transport_in_route.transport_number=transport.transport_number) AS v ON u.route_number = v.route_number );";

      log.info("FA:transportName:"+transportName);      

      String selStop = "SELECT transport_in_route.route_number, stop_name  FROM stop, stop_in_route, transport_in_route, transport WHERE (stop.stop_number=stop_in_route.stop_number AND transport_in_route.route_number=stop_in_route.route_number AND  transport_in_route.transport_number=transport.transport_number AND transport.transport_name='";
      selStop += transportName;
      selStop += "');";

      String selTime="SELECT transport_in_route.route_number, start_time  FROM time_in_route, transport_in_route, transport WHERE (transport_in_route.route_number=time_in_route.route_number AND  transport_in_route.transport_number=transport.transport_number AND transport.transport_name='";
      selTime += transportName;
      selTime += "');";

      String selRoute = "SELECT route.route_number, route_name, route_time FROM transport, transport_in_route, route WHERE (route.route_number=transport_in_route.route_number AND transport.transport_number=transport_in_route.transport_number AND transport.transport_name='";
      selRoute += transportName;
      selRoute += "');";

      log.info("FA:After execute");
      ResultSet rsTransport = stmtTransport.executeQuery(selTransport);
      log.info("FA:After execute transport");
      ResultSet rsStop = stmtStop.executeQuery(selStop);
      log.info("FA:After execute stop");
      ResultSet rsTimes = stmtTime.executeQuery(selTime);
      log.info("FA:After execute time");
      ResultSet rsRoute = stmtRoute.executeQuery(selRoute);
      log.info("FA:After execute all");
      
      int nextNum = -1;

      log.info("FA:Before while");
      while ( rsRoute.next() ) {
        log.info("FA:Before get number");
        nextNum = rsRoute.getInt("route_number");
        log.info("FA:Route while: "+nextNum);

        ArrayList<Transport> transports = new ArrayList<Transport>();
        while ( rsTransport.next() ) {
          if (nextNum == rsTransport.getInt("route_number") ) {
            log.info( "FA:WORK_TR:"+rsTransport.getString("transport_name") );
            Transport transport = new Bus( rsTransport.getString("transport_name") );
            transports.add(transport);
          }
        }

        ArrayList<Stop> stops = new ArrayList<Stop>();
        while ( rsStop.next() ) {
          if (nextNum == rsStop.getInt("route_number") ) {
            Stop stop = new Stop( rsStop.getString("stop_name") );
            stops.add(stop);
          }
        }

        ArrayList<String> times = new ArrayList<String>();
        while ( rsTimes.next() ) {
          if (nextNum == rsTimes.getInt("route_number") ) {
            String time = rsTimes.getString("start_time"); 
            times.add(time);
          }
        }

        log.info("FA:SIZES_TRANSPORTS:"+transports.size());
        log.info("FA:SIZES_TIMES:"+times.size());
        log.info("FA:SIZES_STOPS:"+stops.size());

        Route route = new Route(rsRoute.getString("route_name"), rsRoute.getInt("route_time"),
        stops, transports, times);
        routes.add(route);

      }

      rsTransport.close();
      rsStop.close();
      rsTimes.close();
      rsRoute.close();

      stmtTransport.close();
      stmtStop.close();
      stmtTime.close();
      stmtRoute.close();

      c.close();
    } catch (Exception e) {
      log.info("FA:exception");
      e.printStackTrace();
      log.info(e.toString());

      try {
        stmtTransport.close();
        stmtStop.close();
        stmtTime.close();
        stmtRoute.close();

        c.close();
      } catch (Exception e2) {
        log.info("FA:Error close connection");
      }
    }

    log.info("FA:return");
    return routes;
  }

    //returns routes from DataBase which contain this Stop
  public ArrayList<Route> requestForRoutesByStop(String stopName) {
   
    Logger log = Logger.getLogger("RoutesDaoImpl");        
    log.info("FA:simple");

    Connection c = null;
    Statement stmtStop = null;
    Statement stmtTransport = null;
    Statement stmtTime = null;
    Statement stmtRoute = null;
    ArrayList<Route> routes = new ArrayList<Route>();

    try {
      log.info("FA:Before connect");
      Class.forName("org.postgresql.Driver");
      log.info("FA:After forName");
      c = DriverManager.getConnection("jdbc:postgresql://localhost:5433/goverment", "alex", "password");
      c.setAutoCommit(false);
      log.info("FA:After connect");

      log.info("FA:Before Statement");
      stmtStop = c.createStatement();
      stmtTransport = c.createStatement();
      stmtTime = c.createStatement();
      stmtRoute = c.createStatement();
      log.info("FA:After Statement");      

      String selStop = "SELECT v.route_number, v.stop_name  FROM ( (SELECT route_number FROM stop, stop_in_route WHERE stop.stop_number=stop_in_route.stop_number AND stop.stop_name='";
      selStop += stopName;
      selStop +="') AS u INNER JOIN (SELECT route_number, stop_name FROM stop, stop_in_route WHERE stop_in_route.stop_number=stop.stop_number) AS v ON u.route_number = v.route_number );";

      String selTransport = "SELECT stop_in_route.route_number, transport_name  FROM transport, transport_in_route, stop_in_route, stop WHERE (transport.transport_number=transport_in_route.transport_number AND stop_in_route.route_number=transport_in_route.route_number AND stop_in_route.stop_number=stop.stop_number AND stop.stop_name='";
      selTransport += stopName;
      selTransport += "');";

      String selTime="SELECT stop_in_route.route_number, start_time  FROM time_in_route, stop_in_route, stop WHERE (stop_in_route.route_number=time_in_route.route_number AND  stop_in_route.stop_number=stop.stop_number AND stop.stop_name='";
      selTime += stopName;
      selTime += "');";

      String selRoute = "SELECT route.route_number, route_name, route_time FROM stop, stop_in_route, route WHERE (route.route_number=stop_in_route.route_number AND stop.stop_number=stop_in_route.stop_number AND stop.stop_name='";
      selRoute += stopName;
      selRoute += "');";

      log.info("FA:After execute");
      ResultSet rsStop = stmtStop.executeQuery(selStop);
      log.info("FA:After execute stop");
      ResultSet rsTransport = stmtTransport.executeQuery(selTransport);
      log.info("FA:After execute transport");
      ResultSet rsTimes = stmtTime.executeQuery(selTime);
      log.info("FA:After execute time");
      ResultSet rsRoute = stmtRoute.executeQuery(selRoute);
      log.info("FA:After execute all");
      
      int nextNum = -1;

      log.info("FA:Before while");
      while ( rsRoute.next() ) {
        log.info("FA:Before get number");
        nextNum = rsRoute.getInt("route_number");
        log.info("FA:Route while: "+nextNum);

        ArrayList<Transport> transports = new ArrayList<Transport>();
        while ( rsTransport.next() ) {
          if (nextNum == rsTransport.getInt("route_number") ) {
            Transport transport = new Bus( rsTransport.getString("transport_name") );
            transports.add(transport);
          }
        }

        ArrayList<Stop> stops = new ArrayList<Stop>();
        while ( rsStop.next() ) {
          if (nextNum == rsStop.getInt("route_number") ) {
            Stop stop = new Stop( rsStop.getString("stop_name") );
            stops.add(stop);
          }
        }

        ArrayList<String> times = new ArrayList<String>();
        while ( rsTimes.next() ) {
          if (nextNum == rsTimes.getInt("route_number") ) {
            String time = rsTimes.getString("start_time"); 
            times.add(time);
          }
        }

        log.info("FA:SIZES_TRANSPORTS:"+transports.size());
        log.info("FA:SIZES_TIMES:"+times.size());
        log.info("FA:SIZES_STOPS:"+stops.size());

        Route route = new Route(rsRoute.getString("route_name"), rsRoute.getInt("route_time"),
        stops, transports, times);
        routes.add(route);

      }
      
      rsStop.close();
      rsTransport.close();
      rsTimes.close();
      rsRoute.close();

      stmtStop.close();
      stmtTransport.close();
      stmtTime.close();
      stmtRoute.close();

      c.close();
      
    } catch (Exception e) {
      log.info("FA:exception");
      e.printStackTrace();
      log.info(e.toString());

      try {
      
        stmtTransport.close();
        stmtStop.close();
        stmtTime.close();
        stmtRoute.close();

        c.close();
      } catch (Exception e2) {
        log.info("FA:Error close connection");
      }
    }

    log.info("FA:return");
    return routes;
  }
}
