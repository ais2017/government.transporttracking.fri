package dao;

import objects.Route;

import java.util.ArrayList;

/**
 * Created by ruslbizh on 03.12.2017.
 */
public interface RoutesDao {
    ArrayList<Route> requestForRoutesByTransport(String transportName);
    ArrayList<Route> requestForRoutesByStop(String stopName);
}
