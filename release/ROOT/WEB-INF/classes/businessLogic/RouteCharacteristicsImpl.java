package businessLogic;

import dao.RoutesDaoImpl;
import objects.Route;
import objects.Stop;
import objects.Transport;

import java.util.logging.Logger;

import java.security.InvalidParameterException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by ruslbizh on 19.11.2017.
 */
public class RouteCharacteristicsImpl implements RouteCharacteristics {

    public RouteCharacteristicsImpl() {
        //this.routes = routes;
    }

    public String getCurrentTime(){
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        return sdf.format(cal.getTime());
    }

    public List<Stop> getRouteForTransport(String transportName){
  
        Logger log = Logger.getLogger("RoutesDaoImpl");        
        log.info("FA:simplegetRouteForTransport");

        RoutesDaoImpl dao = new RoutesDaoImpl();
        ArrayList<Route> routes = dao.requestForRoutesByTransport(transportName);
        
        int i = 0;
        for(Route route: routes) {
            log.info("FA:routes:"+i);
            if(route.getTransport(transportName) != null) {
              log.info("FA:routes!=null:"+i);
              return route.getStops();
            }
          log.info("FA:routes==null:"+i);
        }
        return null;
    }

    public Map<String, String> getTransportsTimeToStop(String stopName, String curTime){

        RoutesDaoImpl dao = new RoutesDaoImpl();
        ArrayList<Route> routes = dao.requestForRoutesByStop(stopName);

        TreeMap<String, String> map = new TreeMap<String, String>();
        for(Route route: routes){
            if(route.getStop(stopName) != null){
                ArrayList<Transport> transports = route.getTransports();
                for(Transport transport: transports){
                    int time = route.getTimeToStop(transport.getTransportName(),stopName, curTime);
                    String stringTime = RouteCharacteristics.getStringTime(time);
                    map.put(stringTime, transport.getTransportName());
                }
            }
        }
        return map;
    }
}
