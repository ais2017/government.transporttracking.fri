package businessLogic;

import objects.Route;
import objects.Stop;

import java.security.InvalidParameterException;
import java.util.List;
import java.util.Map;

/**
 * Created by ruslbizh on 03.12.2017.
 */
public interface RouteCharacteristics {

    String getCurrentTime();
    static String getStringTime(int seconds){
        if(seconds > 0) {
            Integer hours = seconds / (60 * 60);
            Integer min = (seconds - hours * 60 * 60) / 60;
            Integer sec = (seconds - hours * 60 * 60 - min * 60);
            String time = null;
            if (hours.toString().length() > 1) {
                time = hours.toString() + ":";
            } else {
                time = "0" + hours.toString() + ":";
            }
            if (min.toString().length() > 1) {
                time += min.toString() + ":";
            } else {
                time += "0" + min.toString() + ":";
            }
            if (sec.toString().length() > 1) {
                time += sec.toString();
            } else {
                time += "0" + sec.toString();
            }
            return time;
        } else {
            throw new InvalidParameterException("parameter should be bigger than 0");
        }
    }
    List<Stop> getRouteForTransport(String transportName);
    Map<String, String> getTransportsTimeToStop(String stopName, String curTime);
}
