package objects;
import objects.Transport;

/**
 * Created by ruslbizh on 03.11.2017.
 */
public class Tram extends Transport{
    private static final double speedCoef = 1;
    private int timeBetweenStops;
    private int generalTimeOfTraffic;

    public Tram(String transportName) {
        setTransportName(transportName);
    }

    public static double getSpeedCoef() {
        return speedCoef;
    }

//  @Override
    public int getTimeBetweenStops() {
        return timeBetweenStops;
    }

//  @Override
    public void setTimeBetweenStops(int generaltimeBetweenStops, int numOfStops){
        this.timeBetweenStops = (int)(generaltimeBetweenStops/speedCoef);
        this.generalTimeOfTraffic = timeBetweenStops*numOfStops;
    }

//  @Override
    public int getGeneralTimeOfTraffic() {
        return generalTimeOfTraffic;
    }
}
