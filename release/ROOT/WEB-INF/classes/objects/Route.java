package objects;
import objects.Stop;
import objects.Transport;

import javafx.util.Pair;
//import org.hibernate.Transaction;

import java.security.InvalidParameterException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.text.SimpleDateFormat;

/**
 * Created by ruslbizh on 03.11.2017.
 */
public class Route {

    private String routeName;
    private ArrayList<Stop> stops = new ArrayList<Stop>();
    private ArrayList<Transport> transports = new ArrayList<Transport>();
    private ArrayList<String> starts = new ArrayList<String>();
    private int generalTimeBetweenStops;

    public Route(String routeName) {
        this.routeName = routeName;
    }

    public Route(String routeName, int generaltimeBetweenStops, ArrayList<Stop> stops, ArrayList<Transport> transports,
                 ArrayList<String> starts) {
        this.routeName = routeName;
        this.generalTimeBetweenStops = generaltimeBetweenStops;
        setStops(stops);
        setTransports(transports);
        setStarts(starts);
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public ArrayList<String> getStarts() {
        return starts;
    }

    public void setStarts(ArrayList<String> starts) {
        if(checkStarts(starts)) {
            this.starts = starts;
            Collections.sort(this.starts,new Comparator<String>() {
                public int compare(String o1, String o2) {
                    return compareTime(o1,o2);
                }
            });
        } else {
            throw new InvalidParameterException("starts.size should equal transports.size");
        }
    }

    public boolean checkStarts(ArrayList<String> starts){
        if(transports.size() != 0 && starts.size() != transports.size()){
            return false;
        }
        Pattern pattern = Pattern.compile("(([0-1][0-9])|([2][0-4])):([0-5][0-9]):([0-5][0-9])");//examples: '06:15:00', '23:20:10'
        for(String elem: starts) {
            Matcher m = pattern.matcher(elem);
            if(!m.matches()){
                return false;
            }
        }
        return true;
    }

    public ArrayList<Stop> getStops() {
        return stops;
    }

    public void setStops(ArrayList<Stop> stops) {
        this.stops = stops;
        if(transports.size() != 0) {
            for (Transport el : transports) {
                el.setTimeBetweenStops(generalTimeBetweenStops, getNumOfStops());
            }
        }
        for (Stop el : this.stops) {
            el.addRoute(this);
        }
    }

    public ArrayList<Transport> getTransports() {
        return transports;
    }

    public void setTransports(ArrayList<Transport> transports) {
        if(starts.size() == transports.size() || starts.size() == 0){
            this.transports = transports;
            int i=0;
            for(Transport el: this.transports){
                el.setStartNumber(i);
                if(getNumOfStops() != 0 && generalTimeBetweenStops!=0) {
                    el.setTimeBetweenStops(generalTimeBetweenStops, getNumOfStops());
                }
                ++i;
            }
        } else {
            throw new InvalidParameterException("starts.size should equal transports.size");
        }
    }

    public int getNumOfStops() {
        return stops.size();
    }

    public int getGeneralTimeBetweenStops() {
        return generalTimeBetweenStops;
    }

    public void setGeneralTimeBetweenStops(int generaltimeBetweenStops) {
        this.generalTimeBetweenStops = generaltimeBetweenStops;
        for(Transport el: transports){
            el.setTimeBetweenStops(generaltimeBetweenStops, getNumOfStops());
        }
    }

    public int getNumOfTransports() {
        return transports.size();
    }

    public Stop getStop(String stopName){
        for(Stop stop: stops){
            if(stop.getStopName().equals(stopName)){
                return stop;
            }
        }
        return null;
    }

    public void addStop(Stop stop){
        if(getStop(stop.getStopName()) == null) {
            stops.add(stop);
        }
    }

    public void delStop(String stopName){
        for(Stop stop: stops){
            if(stop.getStopName().equals(stopName)){
                stops.remove(stop);
                break;
            }
        }
    }

    public boolean setStop(Stop stop){
        int i=0;
        for(Stop st: stops){
            if(st.getStopName().equals(stop.getStopName())){
                stops.set(i, stop);
                return true;
            }
            ++i;
        }
        return false;
    }

    public Transport getTransport(String trName){
        for(Transport transp: transports){
            if(transp.getTransportName().equals(trName)){
                return transp;
            }
        }
        return null;
    }

    public void addTransport(Transport transport){
        if(getTransport(transport.getTransportName())==null) {
            transports.add(transport);
        }
    }

    public void delTransport(String transportName){
        for(Transport trasnp: transports){
            if(trasnp.getTransportName().equals(transportName)){
                transports.remove(trasnp);
                break;
            }
        }
    }

    public boolean setTransport(Transport transport){
        int i=0;
        for(Transport transp: transports){
            if(transp.getTransportName().equals(transport.getTransportName())){
                transports.set(i, transport);
                return true;
            }
            ++i;
        }
        return false;
    }

    public String getStart(int index){
        return starts.get(index);
    }

    public int compareTime(String time1, String time2){
        Pattern pattern = Pattern.compile("(([0-1][0-9])|([2][0-4])):([0-5][0-9]):([0-5][0-9])");
        int h1=0, m1=0, s1=0;
        int h2=0, m2=0, s2=0;
        Matcher matcher = pattern.matcher(time1);
        if(matcher.matches()){
            h1 = Integer.parseInt(matcher.group(1));
            m1 = Integer.parseInt(matcher.group(4));
            s1 = Integer.parseInt(matcher.group(5));
        }
        matcher = pattern.matcher(time2);
        if(matcher.matches()){
            h2 = Integer.parseInt(matcher.group(1));
            m2 = Integer.parseInt(matcher.group(4));
            s2 = Integer.parseInt(matcher.group(5));
        }
        if(h1 > h2) return 1;
        else if(h1 == h2){
            if(m1 > m2) return 1;
            if(m1 == m2){
                if(s1 > s2) return 1;
                else if(s1 == s2) return 0;
                else return -1;
            }
            else return -1;
        }
        else return -1;
    }

    public void addStart(String start){
        boolean flag = true;
        Pattern pattern = Pattern.compile("(([0-1][0-9])|([2][0-4])):([0-5][0-9]):([0-5][0-9])");//examples: '06:15:00', '23:20:10'
        for(String elem: starts) {
            Matcher m = pattern.matcher(start);
            if(!m.matches()){
                flag = false;
            }
        }
        if(flag) {
            for (int i=0; i<starts.size();++i) {
                if (compareTime(start, starts.get(i)) <= 0) {
                    starts.add(i, start);
                    break;
                }
                ++i;
            }
        } else {
            throw new InvalidParameterException("invalid parameter");
        }
    }

    public void delStart(int index){
        starts.remove(index);
    }

    public boolean setStart(int index, String start){
        if(starts.set(index, start).equals(start)) {
            return true;
        }
        Collections.sort(starts,new Comparator<String>() {
            public int compare(String o1, String o2) {
                return compareTime(o1,o2);
            }
        });
        return false;
    }

    public void validateRoute(){
        setStops(this.stops);
        setStarts(this.starts);
        setTransports(this.transports);
    }

/////////////////////////////////////////////////////////////////////////
    private Integer getIndexOfStop(String stopName){
        for(int i=0; i < getNumOfStops(); ++i){
            if(stops.get(i).getStopName().equals(stopName)){
                return i;
            }
        }
        return null;
    }

    private Integer getTimeInSecondsFromMidnight(String formatedTime){
        Pattern pattern = Pattern.compile("(([0-1][0-9])|([2][0-4])):([0-5][0-9]):([0-5][0-9])");
        Matcher matcher = pattern.matcher(formatedTime);
        Integer time = null;
        if(matcher.matches()){
            time = Integer.parseInt(matcher.group(1))*60*60 + Integer.parseInt(matcher.group(4))*60
                    + Integer.parseInt(matcher.group(5));
        }
        return time;
    }

    private Pair<Integer, Integer> findNextAndPrevStop(String transportName, Integer curTime){
        Transport transport = getTransport(transportName);
        String startTime = getStart(transport.getStartNumber());
        int timeBetwStops = transport.getTimeBetweenStops();

        int tmpTime = getTimeInSecondsFromMidnight(startTime);
        int prevStop = -1;
        int nextStop = 0;
        while(tmpTime < curTime){
            if(nextStop == (getNumOfStops()-1)) {
                ++prevStop;
                nextStop = 0;
                tmpTime += timeBetwStops;
                continue;
            }
            if(prevStop == (getNumOfStops()-1)) {
                prevStop = 0;
                ++nextStop;
                tmpTime += timeBetwStops;
                continue;
            }
            ++prevStop;
            ++nextStop;
            tmpTime += timeBetwStops;
        }
        if(curTime == tmpTime){
            prevStop = nextStop;
        }
        return new Pair<Integer, Integer>(prevStop, nextStop);
    }

    public Integer getTimeToNextStop(String transportName, String curTime){
        Transport transport = null;
        for(Transport el : transports){
            if(el.getTransportName().equals(transportName)){
                transport = el;
                break;
            }
        }
        if(transport == null){
            return null;
        }
        Integer currentTime = getTimeInSecondsFromMidnight(curTime);
        Pair<Integer, Integer> pair = findNextAndPrevStop(transportName,currentTime);
        Integer indexPrev = pair.getKey();
        Integer indexNext = pair.getValue();

        Integer timeWhenStart = getTimeInSecondsFromMidnight(getStarts().get(transport.getStartNumber()));
        int timeOfCycle = transport.getTimeBetweenStops() * getNumOfStops();
        int timeOfDrive = currentTime - timeWhenStart;
        int numOfCycles = timeOfDrive/timeOfCycle;
        if(indexNext > indexPrev) {
            return indexNext*transport.getTimeBetweenStops() - (timeOfDrive - timeOfCycle*numOfCycles);
        } else if(indexNext.equals(indexPrev)){
            return 0;
        } else {//if prevStop is endStop and therefore nextStop is startStop
            return timeOfCycle - indexNext*transport.getTimeBetweenStops() - (timeOfDrive - timeOfCycle*numOfCycles);
        }
    }

    public Integer getTimeToStop(String transportName, String stopName, String curTime){
        Integer indexOfDestinationStop = getIndexOfStop(stopName);
        if(indexOfDestinationStop == null){
            return null;
        }
        Transport transport = null;
        for(Transport el : transports){
            if(el.getTransportName().equals(transportName)){
                transport = el;
                break;
            }
        }
        if(transport == null){
            return null;
        }
        Integer currentTime = getTimeInSecondsFromMidnight(curTime);
        Pair<Integer, Integer> pair = findNextAndPrevStop(transportName,currentTime);
        Integer indexNext = pair.getValue();
        if( indexOfDestinationStop >= indexNext) {
            if (indexOfDestinationStop.intValue() == indexNext) {
                return getTimeToNextStop(transportName, curTime);
            } else {
                int timeToNextStop = getTimeToNextStop(transportName, curTime);
                int numOfStopsToDrive = indexOfDestinationStop - indexNext;
                return timeToNextStop + transport.getTimeBetweenStops() * numOfStopsToDrive;
            }
        } else {
            int timeToNextStop = getTimeToNextStop(transportName, curTime);
            int indexOfLastStop = getNumOfStops() - 1;
            int numOfStopsToDrive = indexOfLastStop - indexNext + indexOfDestinationStop + 1;
            return timeToNextStop + transport.getTimeBetweenStops() * numOfStopsToDrive;
        }
    }
}


