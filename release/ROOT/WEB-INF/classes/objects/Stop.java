package objects;
import objects.Route;

import java.util.ArrayList;

/**
 * Created by ruslbizh on 03.11.2017.
 */
public class Stop {

    private String stopName;
    private ArrayList<Route> routes = new ArrayList<Route>();
    private double latitude;
    private double longitude;

    public Stop(String stopName) {
        this.stopName = stopName;
    }

    public Stop(String stopName, double latitude, double longitude, ArrayList<Route> routes) {
        this.stopName = stopName;
        this.latitude = latitude;
        this.longitude = longitude;
        this.routes = routes;
    }

    public Route getRoute(String routeName){
        for(Route r: routes){
            if(r.getRouteName().equals(routeName)){
                return r;
            }
        }
        return null;
    }

    public void addRoute(Route route){
        if(getRoute(route.getRouteName())==null) {
            routes.add(route);
        }
    }

    public void delRoute(String routeName){
        for(Route r: routes){
            if(r.getRouteName().equals(routeName)){
                routes.remove(r);
            }
        }
    }

    public boolean setRoute(Route route){
        int i=0;
        for(Route r: routes){
            if(r.getRouteName().equals(route.getRouteName())){
                routes.set(i,route);
                return true;
            }
            ++i;
        }
        return false;
    }

    public String getStopName() {
        return stopName;
    }

    public void setStopName(String stopName) {
        this.stopName = stopName;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public ArrayList<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(ArrayList<Route> routes) {
        this.routes = routes;
    }
}
